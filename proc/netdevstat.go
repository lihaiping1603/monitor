//The MIT License (MIT)
//
//Copyright (c) 2020 lihp1603
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

package proc

import (
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func ReadNetDevStat() ([]NetDevStat, error) {
	return readNetDevStat("/proc/net/dev")
}

func readNetDevStat(path string) ([]NetDevStat, error) {
	data, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")

	// lines[2:] remove /proc/net/dev header
	results := make([]NetDevStat, len(lines[2:])-1)
	rs := time.Now()
	for i, line := range lines[2:] {
		// patterns
		// <iface>: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		// or
		// <iface>:0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 (without space after colon)
		colon := strings.Index(line, ":")

		if colon > 0 {
			metrics := line[colon+1:]
			fields := strings.Fields(metrics)

			results[i].Iface = strings.Replace(line[0:colon], " ", "", -1)
			results[i].RxBytes, _ = strconv.ParseUint(fields[0], 10, 64)
			results[i].RxPackets, _ = strconv.ParseUint(fields[1], 10, 64)
			results[i].RxErrs, _ = strconv.ParseUint(fields[2], 10, 64)
			results[i].RxDrop, _ = strconv.ParseUint(fields[3], 10, 64)
			results[i].RxFifo, _ = strconv.ParseUint(fields[4], 10, 64)
			results[i].RxFrame, _ = strconv.ParseUint(fields[5], 10, 64)
			results[i].RxCompressed, _ = strconv.ParseUint(fields[6], 10, 64)
			results[i].RxMulticast, _ = strconv.ParseUint(fields[7], 10, 64)
			results[i].TxBytes, _ = strconv.ParseUint(fields[8], 10, 64)
			results[i].TxPackets, _ = strconv.ParseUint(fields[9], 10, 64)
			results[i].TxErrs, _ = strconv.ParseUint(fields[10], 10, 64)
			results[i].TxDrop, _ = strconv.ParseUint(fields[11], 10, 64)
			results[i].TxFifo, _ = strconv.ParseUint(fields[12], 10, 64)
			results[i].TxColls, _ = strconv.ParseUint(fields[13], 10, 64)
			results[i].TxCarrier, _ = strconv.ParseUint(fields[14], 10, 64)
			results[i].TxCompressed, _ = strconv.ParseUint(fields[15], 10, 64)
			//
			results[i].UpdateSampledTime(&rs)
		}
	}

	return results, nil
}

type NetDevStat struct {
	SampledTime
	Iface        string `json:"iface"`
	RxBytes      uint64 `json:"rxbytes"`
	RxPackets    uint64 `json:"rxpackets"`
	RxErrs       uint64 `json:"rxerrs"`
	RxDrop       uint64 `json:"rxdrop"`
	RxFifo       uint64 `json:"rxfifo"`
	RxFrame      uint64 `json:"rxframe"`
	RxCompressed uint64 `json:"rxcompressed"`
	RxMulticast  uint64 `json:"rxmulticast"`
	TxBytes      uint64 `json:"txbytes"`
	TxPackets    uint64 `json:"txpackets"`
	TxErrs       uint64 `json:"txerrs"`
	TxDrop       uint64 `json:"txdrop"`
	TxFifo       uint64 `json:"txfifo"`
	TxColls      uint64 `json:"txcolls"`
	TxCarrier    uint64 `json:"txcarrier"`
	TxCompressed uint64 `json:"txcompressed"`
}

func (nds *NetDevStat) GetIfaceName() string {
	return nds.Iface
}

func (nds *NetDevStat) GetRxBytes() uint64 {
	return nds.RxBytes
}

func (nds *NetDevStat) GetTxBytes() uint64 {
	return nds.TxBytes
}
