//The MIT License (MIT)
//
//Copyright (c) 2020 lihp1603
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

package monitor

import (
	"time"
)

//监控器
type Monitor struct {
	StartTime time.Time    `json:"start_time"`
	SysStat   *SystemStats `json:"system"`
	SelfStat  *SelfStats   `json:"self"`
}

func NewMonitor(disk_catalog, disk_name string) (*Monitor, error) {
	m := &Monitor{StartTime: time.Now()}
	sysstat, err := NewSystemStats(disk_catalog, disk_name)
	if err != nil {
		return nil, err
	}
	selfstat, err := NewSelfStats()
	if err != nil {
		return nil, err
	}
	m.SysStat = sysstat
	m.SelfStat = selfstat
	return m, nil
}

func (m *Monitor) GetSysStats() *SystemStats {
	return m.SysStat
}

func (m *Monitor) GetSelfStats() *SelfStats {
	return m.SelfStat
}
