### 系统cpu使用信息统计:/proc/stat

对于内核活动的信息各个部分信息通过/proc/stat文件进行统计和记录，它记录的是自系统启动以来的数据信息。

```
//先来看下官方的文档介绍:
  > cat /proc/stat
  cpu  2255 34 2290 22625563 6290 127 456 0 0 0
  cpu0 1132 34 1441 11311718 3675 127 438 0 0 0
  cpu1 1123 0 849 11313845 2614 0 18 0 0 0
  intr 114930548 113199788 3 0 5 263 0 4 [... lots more numbers ...]
  ctxt 1990473
  btime 1062191376
  processes 2915
  procs_running 1
  procs_blocked 0
  softirq 183433 0 21755 12 39 1137 231 21459 2263

第一行的cpu统计信息是后面所有的cpuN统计信息数据的综合。这些数值标示了cpu在不同种类工作任务中的时间消耗。时间单位为USER_HZ(每秒100次)。其中每个列含义，从左到右依次介绍如下:
- user: 普通进程在用户模式下的执行时间
- nice: 被提高优先级的进程在用户模式下的执行时间
- system: 进程在内核模式下的执行时间
- idle:   空转的时间
- iowait: 等待IO完成的时间. 但是这里涉及到几个问题:
  1.  cpu不会去等待io完成，iowait的时间是任务等待IO完成的时间.其实原因很简单，内核的调度机制中，cpu这么宝贵的资源不可能因为这个任务在执行过程中遇到IO等待的情况，直接干等，而会直接调度去干其他的问题。
  2. 在多核CPU中，等待I/O完成的任务不在任何CPU上运行，因此每个CPU的iowait很难计算.
  3. 在某些条件下，/proc/stat中的iowait字段值会降低。
  所以/proc/stat下的iowait是不可信赖的。
- irq: 处理请求中断所花费的时间
- softirq: 处理软中断请求所花费的时间
- steal: 无意识的等待所花费的时间
- guest: running a normal guest
- guest_nice: running a niced guest

"intr" 一行给出了从启动到现在的中断服务次数，每一次可能的系统中断。第一列是总的中断服务次数；其它后续的列是各种特殊的中断总数。
"ctxt" 一行给出的是从启动到现在所有的CPU进行上下文切换的总次数。
"btime" 一行给出的是系统启动的时间，从Unix纪元（1970年1月1日0时0秒）开始的秒数。
"processes"  一行给出的是创建的进程和线程总数，包括（但不限于）通过调用fork()和clone()系统调用创建的那些进程和线程。
"procs_running"  给出的是当前正在运行的进程数。
"procs_blocked"  给出的是当前被阻塞的进程数，正在等待I/O完成
```

这里需要注意一下时间单位USER_HZ(每秒100次).





### 系统负载信息:/proc/loadavg

```
[live@localhost ~]$ cat /proc/loadavg 
0.04 0.03 0.05 1/242 8934
```

从左至右的内容分别是系统过去1分钟的平均负载、过去5分钟的平均负载、过去15分钟的平均负载、正在运行的任务数/当前总任务数、上一次创建进程使用的PID号。



### 系统运行时间信息:/proc/uptime内容学习

这个文件记录了系统启动后的两个时间(单位为s):

```tex
[live@localhost ~]$ cat /proc/uptime 
604888.88 7226022.93

#第一个时间为自系统启动到现在的时间，代表系统的运行时间num1;
#第二个时间为系统运行过程中的空闲时间num2；
```

这里需要注意一下，在多核系统中，第二个系统空闲时间可能远远>系统的运行时间，因为它将多核的空闲时间进行了累加操作，所以:

**系统的空闲率(%) = num2/(num1\*N) 其中N是多核CPU的系统中的CPU个数。**





### 虚拟内存统计信息: /proc/vmstat 

这个文件主要是记录从内核导出的虚拟内存的统计信息。因为参数比较多，这里主要捡几个常用的参数进行说明:

```tex
pgpgin 2433258         #从启动到现在读入的内存页数 

pgpgout 2157211     #从启动到现在换出的内存页数 

pswpin 43240           #从启动到现在读入的交换分区页数 

pswpout 69525        #从启动到现在换出的交换分区页数
```

**(这里单位为byte)**



### 物理内存统计信息:/proc/meminfo 

这个文件是从内核导出的当前内存的使用情况。下面是一些关键项的解释：

```tex
MemTotal: 515668 kB  #总的物理内存大小
MemFree: 2684 kB 
#可用物理内存大小，因为大量的内存被用于高速缓存，所以这个数比较小，这个值等于下面的HighFree + LowFree的值
Buffers: 8928 kB #缓冲区大小
Cached: 225684 kB #用于高速缓存的大小
SwapCached: 74196 kB #用于高速缓存的交换分区大小
Active: 412920 kB #活动内存量
Inactive: 73988 kB #不活动内存量
HighTotal: 0 kB #高阶内存总数，高阶内存是指超过860M（大约）物理内存以后的内存
HighFree: 0 kB #高阶内存可用内存总数
LowTotal: 515668 kB #低阶内存总数
LowFree: 2684 kB #低阶内存区域的可用内存总数，这是内核可以直接寻址的内存
SwapTotal: 522072 kB #交换分区大小
SwapFree: 365588 kB #可用交换分区大小
Dirty: 28 kB #脏内存，可能要写到磁盘或者交换分区的内存
Writeback: 0 kB
Mapped: 360956 kB
Slab: 17244 kB
Committed_AS: 947452 kB #最坏情况下使用的内存数的一个估计值
PageTables: 3704 kB #内存页表数
VmallocTotal: 499704 kB
VmallocUsed: 3088 kB
VmallocChunk: 496356 kB
HugePages_Total: 0
HugePages_Free: 0
Hugepagesize: 4096 kB
```



### 硬盘/磁盘统计信息: /proc/diskstat 

这个文件主要是统计相关硬盘和磁盘的信息:

```tex
[live@localhost ~]$ cat /proc/diskstats 
   8       0 sda 17985 104 1868284 101108 66226 1532 2594720 972153 0 445570 1073229
   8       1 sda1 22 0 176 494 0 0 0 0 0 494 494
   8       2 sda2 2101 9 49525 6132 16 0 4177 137 0 4973 6268
   8       3 sda3 15770 95 1814271 93817 37911 1532 2590543 785381 0 270711 879172
  11       0 sr0 0 0 0 0 0 0 0 0 0 0 0
 253       0 dm-0 5655 0 488990 62236 14160 0 344704 302647 0 155339 364884
 253       1 dm-1 94 0 4456 1259 0 0 0 0 0 1196 1259
 259       0 nvme0n1 43 0 2136 15 0 0 0 0 0 9 15
 253       2 dm-2 10035 0 1320145 32042 43456 0 2245839 729379 0 292291 761426

从左到右，前面三个依次为主设备号，次设备号，设备名。
后续的11个域分别表示统计信息,依次为:
第1个域：读磁盘的次数，成功完成读的总次数。
第2个域：合并读次数。第6个域 – 合并写次数。为了效率可能会合并相邻的读和写。从而两次4K的读在它最终被处理到磁盘上之前可能会变成一次8K的读，才被计数（和排队），因此只有一次I/O操作。这个域使你知道这样的操作有多频繁。
第3个域：读扇区的次数，成功读过的扇区总次数。
第4个域：读花费的毫秒数，这是所有读操作所花费的毫秒数（用__make_request()到end_that_request_last()测量）。
第5个域：写完成的次数，成功写完成的总次数。
第7个域：写扇区的次数，成功写扇区总次数。
第8个域：写花费的毫秒数，这是所有写操作所花费的毫秒数（用__make_request()到end_that_request_last()测量）。
第9个域：I/O的当前进度，只有这个域应该是0。当请求被交给适当的request_queue_t时增加和请求完成时减小。
第10个域：花在I/O操作上的毫秒数，这个域会增长只要field 9不为0。
第11个域：加权， 花在I/O操作上的毫秒数，在每次I/O开始，I/O结束，I/O合并时这个域都会增加。这可以给I/O完成时间和存储那些可以累积的提供一个便利的测量标准。

```



### 网卡流量统计信息: /proc/net/dev

/proc/net目录用于统计网络信息，这里我们先对这个目录下的子目录和设备文件进行介绍一下:

```tex
/proc/net目录信息介绍:
..............................................................................
 File          Content                                                         
 arp           Kernel  ARP table                                               
 dev           network devices with statistics                                 
 dev_mcast     the Layer2 multicast groups a device is listening too
               (interface index, label, number of references, number of bound
               addresses). 
 dev_stat      network device status                                           
 ip_fwchains   Firewall chain linkage                                          
 ip_fwnames    Firewall chain names                                            
 ip_masq       Directory containing the masquerading tables                    
 ip_masquerade Major masquerading table                                        
 netstat       Network statistics                                              
 raw           raw device statistics                                           
 route         Kernel routing table                                            
 rpc           Directory containing rpc info                                   
 rt_cache      Routing cache                                                   
 snmp          SNMP data                                                       
 sockstat      Socket statistics                                               
 tcp           TCP  sockets                                                    
 udp           UDP sockets                                                     
 unix          UNIX domain sockets                                             
 wireless      Wireless interface data (Wavelan etc)                           
 igmp          IP multicast addresses, which this host joined                  
 psched        Global packet scheduler parameters.                             
 netlink       List of PF_NETLINK sockets                                      
 ip_mr_vifs    List of multicast virtual interfaces                            
 ip_mr_cache   List of multicast routing cache                                 
..............................................................................
```

**这里介绍下/proc/net/dev**

```tex
[live@localhost ~]$ cat /proc/net/dev
Inter-|   Receive                                                |  Transmit
 face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
   br0: 16477159972 31763397    0 1138    0     0          0         0 8851367297 4603094    0    0    0     0       0          0
  eno1: 17123451919 33035047    0    0    0     0          0   8782924 9204661465 9966735    0    0    0     0       0          0
    lo: 22732950089 8107794    0    0    0     0          0         0 22732950089 8107794    0    0    0     0       0          0
virbr0-nic:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
virbr0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
 vnet0:  581413    6274    0    0    0     0          0         0 1382695258 21069345    0    0    0     0       0          0

#接收
收到的字节数 
收到的数据包总数
收到的误码数
收到的丢失误码数
收到的FIFO误码数
收到的帧误码
收到的压缩字节数
收到的多播误码数

#发送
传输的字节数
传输的数据包总数
传输误码总数
传输丢失误码
传输FIFO误码
传输冲突误码 
传输载波误码
传输压缩字节数
```



